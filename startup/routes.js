const express = require('express');
const user = require('../routes/users');
const BodyParser = require('body-parser');
const auth = require('../routes/auth');
const product = require('../routes/product');
const error = require('../middleware/error');

module.exports = function (app) {
    app.use(express.json());
    app.use(BodyParser.json());
    app.use(BodyParser.urlencoded({ extended: true }));
    app.use('/api/product', product);
    app.use('/api/user', user);
    app.use('/api/auth', auth);
    app.use(error);
}