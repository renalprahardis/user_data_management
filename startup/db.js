const mongoose = require('mongoose');
const db = require('../config/keys_dev').mongoURI

module.exports = function () {
    mongoose.set('useCreateIndex', true);
    mongoose.connect(db, { useNewUrlParser: true })
        .then(() => console.log('Connected to database'))
        .catch(err => console.error('Could not connect to MongoDB ..', err))


}