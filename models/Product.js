const mongoose = require('mongoose');
const Joi = require('joi');

const ProductSchema = mongoose.Schema( {
    name : {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true
    }, 
    price: {
        type: Number,
        required: true
    }
});

const Product = mongoose.model('Product', ProductSchema);

function validateProduct(Product){
    const schema = {
        name: Joi.string().min(3).max(15).required(),
        category: Joi.string().min(3).max(255).required(),
        price: Joi.number().required()
        // password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(Product, schema)
};

exports.Product = Product;
exports.validate = validateProduct;