const mongoose = require('mongoose');
const Joi = require('joi');
const PasswordComplexity = require('joi-password-complexity');
const jwt = require('jsonwebtoken');
const config = require('config');

const UserSchema = new mongoose.Schema ({
    name : {
        type: String,
        required: true,
        minlenght: 3,
        maxlegth: 15 
    },
    email : {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: Boolean,
    roles: [],
    operations: []
});

UserSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ _id: this._id, isAdmin: this.isAdmin }, config.get('jwtPrivateKey'));
    return token;
}
const User = mongoose.model('User', UserSchema);

const complexityOptions = {
    min: 5,
    max: 250,
    lowerCase: 1,
    upperCase: 1,
    numeric: 1,
    symbol: 1,
    requirementCount: 2,
};


function validateUser(User){
    const schema = {
        name: Joi.string().min(3).max(15).required(),
        email: Joi.string().min(5).max(255).required(),
        password: new PasswordComplexity(complexityOptions).required(),
        // isAdmin: Joi.boolean
        // password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(User, schema)
};

exports.User = User;
exports.validate = validateUser;