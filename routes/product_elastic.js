const express = require('express');
const router = express.Router();
const User = require('../models/User');
const elasticsearch = require('elasticsearch');

const client = new elasticsearch.Client({
    host: 'http://localhost:9201',
    log: 'trace'
});

router.get("/all", async (req, res) => {
    try {
        const result = await User.find().exec();
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post("/add", async (req, res) => {
    try { 
        const users = new User(req.body);
        const result = await users.save();
        // res.send(result);
        client.index({
            index: 'user',
            type: '_doc',
            body: req.body
        }, (err, resp, status) => {
            if(err){
                console.log(err);
            }else{
                return res.status(200).send({
                    message: 'POST user success'
                })
            }
        });
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get("/:id", async (req, res) => {
    try {
        // const users = await User.findById(req.params.id).exec();
        let user;
        // res.send(users);
        client.get({
            index: 'user',
            type: '_doc',
            id: req.params.id
        }, (err, resp, status) => {
            if(err){
                console.log(err);
            }else{
                user = resp._source;
                console.log('Found the request document', resp);
                if(!user){
                    return res.status(400).send({
                        message: `User is not found for id ${req.params.id}`
                    });
                }
                return res.status(200).send({
                    message: `GET user call for id ${req.params.id} success`
                });
            }
        })
    } catch (error) {
        res.status(500).send(error);
    }
});

router.put("/:id", async (req, res) => {
    try {
        const users = await User.findById(req.params.id).exec();
        users.set(req.body);
        const result = await users.save();
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});

router.delete("/:id", async (req, res) => {
    try {
        const result = User.deleteOne({ _id: req.params.id }).exec();
        // res.send(result);
        client.delete({
            index : 'user',
            type : '_doc',
            id : req.params.id
        }, (err, resp, status) => {
            if(err){
                console.log(err);
            }else{
                user = resp._source;
                // console.log('Found the request document', resp);
                if(!user){
                    return res.status(400).send({
                        message: `User is not found for id ${req.params.id}`
                    });
                }
                return res.status(200).send({
                    message: `DELETE user call for id ${req.params.id} success`
                });
            }
        })
    } catch (error) {
        res.status(500).send(error);
    }
});

router.get("/_search", async (req, res) => {
    try {
        const res = await client.search({
          q: 'renal'
        });
        console.log(res.hits.hits)
      } catch (error) {
        console.trace(error.message)
    }
});


module.exports = router;
