const express = require('express');
const router = express.Router();
const {Product, validate} = require('../models/Product');
const _ = require('lodash');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');


router.get("/", async (req, res) => {
    // throw new Error('Could not get the products.');
    const result = await Product.find().sort('name');
    // console.log(JSON.stringify(result));
    res.send(result);
});
 
router.post('/',auth, async(req,res) =>{
    const {error} = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    let products = await Product.findOne({name: req.body.name})
    if(products) return res.status(400).send('Product name already exists');

    products = new Product (_.pick(req.body, ['name', 'category', 'price']));
    await products.save();
    res.send(products);
});


router.get("/:id", async (req, res) => {
    try {
        const products = await Product.findById(req.params.id).exec();
        res.send(products);
    } catch (ex) {
        res.status(500).send('Product cannot find');
    }
});

router.put("/:id", async (req, res) => {
    try {
        const products = await Product.findById(req.params.id).exec();
        products.set(req.body);
        const result = await products.save();
        res.send(result);
    } catch (ex) {
        res.status(500).send('Product cannot find');
    }
});

router.delete("/:id", [auth, admin], async (req, res) => {
    try {
        const result = Product.deleteOne({ _id: req.params.id }).exec();
        res.send(result);
    } catch (ex) {
        res.status(500).send('Product cannot find');
    }
});

module.exports = router;
