const express = require('express');
const elasticsearch = require('elasticsearch');
const path = require('path');
const user = require('./routes/users');
const app = express();
const port = 8000;

require('./startup/routes')(app);
require('./startup/db')();
require('./startup/logging');
require('./startup/config'); 
require('./startup/validation');

const client = new elasticsearch.Client({
    host: 'http://localhost:9201',
    log: 'trace'
});

// app.use('/api/user', user);

app.get('/', (req, res) => {
    res.send('Hello Renal');
});



app.listen(port, console.log(`Server running on port ${port}`));
